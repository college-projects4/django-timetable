import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator, MinLengthValidator
import re

class User(AbstractUser):
    pass

class PizzaSize(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class PizzaCrust(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class PizzaSauce(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class PizzaCheese(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Pizza(models.Model):
    id = models.AutoField(primary_key=True)
    size = models.ForeignKey(PizzaSize, on_delete=models.CASCADE, null=True)
    crust = models.ForeignKey(PizzaCrust, on_delete=models.CASCADE, null=True)
    sauce = models.ForeignKey(PizzaSauce, on_delete=models.CASCADE, null=True)
    cheese = models.ForeignKey(PizzaCheese, on_delete=models.CASCADE, null=True)
    pepperoni = models.BooleanField(default=False)
    chicken = models.BooleanField(default=False)
    ham = models.BooleanField(default=False)
    pineapple = models.BooleanField(default=False)
    peppers = models.BooleanField(default=False)
    mushrooms = models.BooleanField(default=False)
    onions = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)

    def __str__(self):
        order = f'Size: {self.size} Crust: {self.crust} Sauce: {self.sauce} Cheese: {self.cheese} Pepperoni: {self.pepperoni} Chicken: {self.chicken} Ham: {self.ham} Pineapple: {self.pineapple} Peppers: {self.peppers} Mushrooms: {self.mushrooms} Onions: {self.onions}'
        order = re.sub(r'\w+: False\s?','',order)
        order = re.sub(r'(\s\S*?)\s', r'\1, ', order)
        return order

class Order(models.Model):
    id = models.AutoField(primary_key=True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    name_on_card = models.CharField(max_length=50, blank=False)
    shipping_addr = models.TextField(default="", blank=False)
    card_number = models.CharField(max_length=16, validators=[RegexValidator(r'^\d+$'), MinLengthValidator(16)])
    YEAR_CHOICES = [(y,y) for y in range(datetime.date.today().year, datetime.date.today().year+50)]
    MONTH_CHOICES = [(m,m) for m in range(1,13)]
    card_expiry_year = models.IntegerField(choices=YEAR_CHOICES, default=datetime.datetime.now().year)
    card_expiry_month = models.IntegerField(choices=MONTH_CHOICES, default=datetime.datetime.now().month)
    card_cvv = models.CharField(max_length=3, validators=[RegexValidator(r'^\d+$'), MinLengthValidator(3)])
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)

    def __str__(self):
        return self.name_on_card + " " + str(self.date_ordered)