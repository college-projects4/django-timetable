from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic import CreateView
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView
import re

@login_required
def index(request):
    if request.method == 'POST':
        # the user has pressed "submit" and is sending us data to store
        form = PizzaForm(request.POST)
        if form.is_valid():
            # if the form is valid
            pizza = form.save()
            pizza_id = pizza.id
            request.session['pizza_id'] = pizza_id
            return redirect('/order')
        else:
            return render(request, 'index.html', {'form': form})
    else:
        # if the user is trying to view the form
        form = PizzaForm()
        return render(request, 'index.html', {'form': form})

@login_required
def order(request):
    if request.method == 'POST':
        # the user has pressed "submit" and is sending us data to store
        form = OrderForm(request.POST)
        if form.is_valid():
            # if the form is valid
            order = form.save(commit=False)
            order.user_id = request.user
            pizza_id = request.session.get('pizza_id')
            pizza_obj = Pizza.objects.get(id = pizza_id)
            pizza_obj.paid = True
            pizza_obj.save()
            order.pizza = Pizza.objects.get(id = pizza_id)
            order.save()
            return redirect('/order_complete')
        else:
            pizza_order = Pizza.objects.get(id = request.session.get('pizza_id')).__str__()
            pizza_order = pizza_order.split(',')
            pizza_order.append('Cost: €15')
            return render(request, 'order.html', {'form': form, 'pizza': pizza_order})
    else:
        # if the user is trying to view the form
        form = OrderForm()
        pizza_order = Pizza.objects.get(id = request.session.get('pizza_id')).__str__()
        pizza_order = pizza_order.split(',')
        pizza_order.append('Cost: €15')
        return render(request, 'order.html', {'form': form, 'pizza': pizza_order})

@login_required
def order_complete(request):
    pizza_order = Pizza.objects.get(id = request.session.get('pizza_id')).__str__()
    pizza_order = pizza_order.split(',')
    pizza_order.append('Cost: €15')
    details = Order.objects.filter(user_id = request.user).latest('id')
    return render(request, 'order_complete.html', {'pizza': pizza_order, 'details': details})

@staff_member_required
def order_list(request):
    orders = Order.objects.all()
    return render(request, 'order_list.html', {'orders': orders})

class UserSignupView(CreateView):
    model = User
    form_class = UserSignupForm
    template_name = 'user_signup.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('/')

class UserLoginView(LoginView):
    template_name='login.html'

@login_required
def order_history(request):
    user = request.user
    orders = Order.objects.filter(user_id=user)
    return render(request, 'order_history.html', {'orders':orders})

def logout_user(request):
    logout(request)
    return redirect("/")