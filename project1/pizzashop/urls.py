from django.urls import path
from . import views, forms

urlpatterns = [
    path('', views.index, name="index"),
    path('order/', views.order, name="order"),
    path('order_complete/', views.order_complete, name="order_complete"),
    path('register/', views.UserSignupView.as_view(), name="register"),
    path('login/', views.LoginView.as_view(template_name="login.html", authentication_form=forms.UserLoginForm)),
    path('logout/', views.logout_user, name="logout"),
    path('orderhistory/', views.order_history, name="order_history"),
    path('orderlist/', views.order_list, name="order_list"),
]