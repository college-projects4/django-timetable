# Generated by Django 4.1.1 on 2022-10-17 19:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pizzashop', '0002_pizza'),
    ]

    operations = [
        migrations.CreateModel(
            name='PizzaSize',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=150)),
            ],
        ),
        migrations.AlterField(
            model_name='pizza',
            name='size',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='pizzashop.pizzasize'),
        ),
    ]
