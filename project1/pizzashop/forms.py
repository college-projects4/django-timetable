from django.forms import  ModelForm
from .models import *
from django import forms
from django.db import transaction
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm



class UserSignupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_admin = False
        user.save()
        return user

class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Your username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Your password'}))

class PizzaForm(ModelForm):
    class Meta:
        model = Pizza
        fields = ['size', 'crust' ,'sauce', 'cheese', 'pepperoni', 'chicken', 'ham', 'pineapple', 'peppers', 'mushrooms', 'onions']

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['name_on_card', 'shipping_addr', 'card_number', 'card_expiry_year', 'card_expiry_month', 'card_cvv']