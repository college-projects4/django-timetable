# Generated by Django 4.1.1 on 2022-11-09 18:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Timetable', '0009_remove_student_cohort'),
    ]

    operations = [
        migrations.AddField(
            model_name='cohort',
            name='student',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='Timetable.student'),
            preserve_default=False,
        ),
    ]
