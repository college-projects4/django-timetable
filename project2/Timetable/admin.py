from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

admin.site.register(User, UserAdmin)
admin.site.register(Cohort)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Module)
admin.site.register(Room)
admin.site.register(Booking)
admin.site.register(RoomBooking)