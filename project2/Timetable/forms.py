from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django import forms
from .models import * 
from django.db import transaction
from django.forms import ModelForm


class StudentSignupform(UserCreationForm):
    cohort = forms.ModelChoiceField(queryset=Cohort.objects.all())
    class Meta(UserCreationForm.Meta):
        model = User
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_student = True
        user.save()
        return user


class TeacherSignupform(UserCreationForm):
    module = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=[], required=False)
    class Meta(UserCreationForm.Meta):
        model = User
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_student = False
        user.save()
        return user

    def __init__(self, *args, **kwargs):
        M = Module.objects.filter(has_teacher=False)
        OPTIONS = []
        for choice in M:
            OPTIONS.append((choice.code, choice.code))
        super(TeacherSignupform, self).__init__(*args, **kwargs)
        self.fields['module'].choices = OPTIONS

class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Your username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Your password'}))

class BookingForm(ModelForm):
    class Meta:
        model = Booking
        fields = ['day', 'start_time','duration', 'type', 'room', 'module']

class RoomBookingForm(ModelForm):
    class Meta:
        model = RoomBooking
        fields = ['day', 'start_time','duration', 'room']
