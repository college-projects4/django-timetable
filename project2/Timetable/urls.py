from django.urls import path
from . import views, forms

urlpatterns = [
    path('', views.index, name='index'),
    path('registerstudent/', views.StudentSignupView.as_view(), name='student_register'),
    path('registerteacher/', views.TeacherSignupView.as_view(), name="register_teacher"),
    path('login/', views.LoginView.as_view(template_name="login.html", authentication_form=forms.UserLoginForm)),
    path('logout/', views.logout_user, name="logout"),
    path('createbooking/', views.CreateBooking, name="createbooking"),
    path('roombooking/', views.CreateRoomBooking, name="roombooking"),
    path('bookingconfirmation/', views.BookingConfirmed, name="bookingconfirmation"),
    path('roombookingconfirmation/', views.RoomBookingConfirmed, name="roombookingconfirmation"),
]