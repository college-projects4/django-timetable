from django.shortcuts import render, redirect
from .forms import *
from .models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.generic import CreateView
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from django.core import serializers

@login_required
def index(request):
    user = request.user
    days_choices = Booking.DAY_CHOICES
    days = []
    for day in days_choices:
        days.append(day[0])
    time_choices = Booking.HOUR_CHOICES
    times = []
    for time in time_choices:
        times.append(time[1])
    if user.is_superuser:
        return render(request, 'admin_index.html')
    elif user.is_student:
        student = Student.objects.get(user=user)
        cohort = student.cohort
        timetable = Booking.objects.filter(module__cohort=cohort)
        bookings = serializers.serialize("json", timetable)
        timetable_rooms = RoomBooking.objects.filter(user=user)
        room_bookings = serializers.serialize("json", timetable_rooms)
        rooms = serializers.serialize("json",Room.objects.all())
        modules = serializers.serialize("json",Module.objects.all())
        cohorts = serializers.serialize("json",Cohort.objects.all())
        return render(request, 'index.html', {'days': days, 'times': times, 'bookings': bookings, 'rooms': rooms, 'modules': modules, 'cohorts': cohorts, 'room_bookings': room_bookings})
    else:
        teacher = Teacher.objects.get(user=user)
        timetable = Booking.objects.filter(module__teacher=teacher)
        bookings = serializers.serialize("json", timetable)
        timetable_rooms = RoomBooking.objects.filter(user=user)
        room_bookings = serializers.serialize("json", timetable_rooms)
        rooms = serializers.serialize("json",Room.objects.all())
        modules = serializers.serialize("json",Module.objects.all())
        cohorts = serializers.serialize("json",Cohort.objects.all())
        return render(request, 'index.html', {'days': days, 'times': times, 'bookings': bookings, 'rooms': rooms, 'modules': modules, 'cohorts': cohorts, 'room_bookings': room_bookings})

class StudentSignupView(CreateView):
    model = User
    form_class = StudentSignupform
    template_name = 'signup.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        cohort_obj = Cohort.objects.get(name=[form.cleaned_data['cohort']][0])
        student = Student.objects.create(user=self.request.user, cohort=cohort_obj)
        student.save()
        return redirect('/')

class TeacherSignupView(CreateView):
    model = User
    form_class = TeacherSignupform
    template_name = 'signup.html'

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        module_code = [form.cleaned_data['module']][0]
        teacher = Teacher.objects.create(user=self.request.user)
        if module_code:
            modules = []
            for single_module in module_code:
                module_obj = Module.objects.get(code=single_module)
                module_obj.has_teacher = True
                module_obj.save()
                modules.append(module_obj.id)
            teacher.module.add(*modules)
        teacher.save()
        return redirect('/')

@login_required
def CreateBooking(request):
    user = request.user
    if user.is_student and not user.is_superuser:
        return HttpResponseNotFound('<h1>Page not found</h1><br> <a href="/"><button>Homepage</button></a>')
    else:
        if request.method == 'POST':
            form = BookingForm(request.POST)
            if form.is_valid():
                booking = form.save(commit=False)
                all_bookings = Booking.objects.all()
                for single_booking in all_bookings:
                    if single_booking.day == booking.day:
                        singlebooking_starttime = single_booking.start_time.hour
                        booking_starttime = booking.start_time.hour
                        singlebooking_endtime = single_booking.start_time.hour + single_booking.duration
                        booking_endtime = booking.start_time.hour + booking.duration
                        if (singlebooking_starttime <= booking_starttime < singlebooking_endtime) or (singlebooking_starttime < booking_endtime < singlebooking_endtime) or (booking_starttime <= singlebooking_starttime < booking_endtime) or (booking_starttime < singlebooking_endtime < booking_endtime):
                            if not user.is_superuser:
                                teacher = Teacher.objects.get(user=request.user)
                                single_bookingteacher = Teacher.objects.get(user=single_booking.user)
                                if single_bookingteacher == teacher:
                                    error = "There is already a booking scheduled at this time for this teacher"
                                    return render(request, 'create_booking.html', {'form': form, 'error':error})
                            elif booking.module.cohort == single_booking.module.cohort:
                                error = "There is already a booking scheduled at this time for this cohort"
                                return render(request, 'create_booking.html', {'form': form, 'error':error})
                            elif booking.room == single_booking.room:
                                error = "There is already a booking scheduled at this time in this room"
                                return render(request, 'create_booking.html', {'form': form, 'error':error})
                all_bookings = RoomBooking.objects.all()
                for single_booking in all_bookings:
                    if single_booking.day == booking.day:
                        singlebooking_starttime = single_booking.start_time.hour
                        booking_starttime = booking.start_time.hour
                        singlebooking_endtime = single_booking.start_time.hour + single_booking.duration
                        booking_endtime = booking.start_time.hour + booking.duration
                        if (singlebooking_starttime <= booking_starttime < singlebooking_endtime) or (singlebooking_starttime < booking_endtime < singlebooking_endtime) or (booking_starttime <= singlebooking_starttime < booking_endtime) or (booking_starttime < singlebooking_endtime < booking_endtime):
                            if booking.room == single_booking.room:
                                error = "There is already a booking scheduled at this time in this room"
                                return render(request, 'create_booking.html', {'form': form, 'error':error})
                            if single_booking.user == request.user:
                                error = "You have another booking scheduled at this time"
                                return render(request, 'create_booking.html', {'form': form, 'error':error})
                booking.user = request.user
                booking = form.save()
                return redirect('/bookingconfirmation')
            else:
                form = BookingForm()
                if not user.is_superuser:
                    teacher = Teacher.objects.get(user=user)
                    modules = Module.objects.filter(teacher = teacher)
                    form.fields["module"].queryset = modules
                return render(request, 'create_booking.html', {'form': form})
        else:
            form = BookingForm()
            if not user.is_student and not user.is_superuser:
                teacher = Teacher.objects.get(user=user)
                modules = Module.objects.filter(teacher = teacher)
                if not modules.exists():
                    error = "You aren't teaching any modules so cannot make a booking"
                    return render(request, 'create_booking_error.html', {'error':error})
                form.fields["module"].queryset = modules
            return render(request, 'create_booking.html', {'form': form})

@login_required
def CreateRoomBooking(request):
    if request.method == 'POST':
        form = RoomBookingForm(request.POST)
        if form.is_valid():
            booking = form.save(commit=False)
            all_bookings = Booking.objects.all()
            for single_booking in all_bookings:
                if single_booking.day == booking.day:
                    singlebooking_starttime = single_booking.start_time.hour
                    booking_starttime = booking.start_time.hour
                    singlebooking_endtime = single_booking.start_time.hour + single_booking.duration
                    booking_endtime = booking.start_time.hour + booking.duration
                    if (singlebooking_starttime <= booking_starttime < singlebooking_endtime) or (singlebooking_starttime < booking_endtime < singlebooking_endtime) or (booking_starttime <= singlebooking_starttime < booking_endtime) or (booking_starttime < singlebooking_endtime < booking_endtime):
                        if booking.room == single_booking.room:
                            error = "There is already a booking scheduled at this time in this room"
                            return render(request, 'create_booking.html', {'form': form, 'error':error})
                        if single_booking.user == request.user:
                            error = "You have another booking scheduled at this time"
                            return render(request, 'create_booking.html', {'form': form, 'error':error})
            all_bookings = RoomBooking.objects.all()
            for single_booking in all_bookings:
                if single_booking.day == booking.day:
                    singlebooking_starttime = single_booking.start_time.hour
                    booking_starttime = booking.start_time.hour
                    singlebooking_endtime = single_booking.start_time.hour + single_booking.duration
                    booking_endtime = booking.start_time.hour + booking.duration
                    if (singlebooking_starttime <= booking_starttime < singlebooking_endtime) or (singlebooking_starttime < booking_endtime < singlebooking_endtime) or (booking_starttime <= singlebooking_starttime < booking_endtime) or (booking_starttime < singlebooking_endtime < booking_endtime):
                        if booking.room == single_booking.room:
                            error = "There is already a booking scheduled at this time in this room"
                            return render(request, 'create_booking.html', {'form': form, 'error':error})
                        if single_booking.user == request.user:
                            error = "You have another booking scheduled at this time"
                            return render(request, 'create_booking.html', {'form': form, 'error':error})
            booking.user = request.user
            booking = form.save()
            return redirect('/roombookingconfirmation')
        else:
            form = RoomBookingForm()
            return render(request, 'create_booking.html', {'form': form})
    else:
        form = RoomBookingForm()
        return render(request, 'create_booking.html', {'form': form})

@login_required
def BookingConfirmed(request):
    booking_details = Booking.objects.filter(user=request.user).latest('id')
    return render(request, 'booking_confirmed.html', {'booking_details': booking_details})

@login_required
def RoomBookingConfirmed(request):
    booking_details = RoomBooking.objects.filter(user=request.user).latest('id')
    return render(request, 'roombooking_confirmed.html', {'booking_details': booking_details})

class UserLoginView(LoginView):
    template_name='login.html'

def logout_user(request):
    logout(request)
    return redirect("/")