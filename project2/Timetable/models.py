from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime

class User(AbstractUser):
    is_student = models.BooleanField(default=True)

class Cohort(models.Model): #EC4
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Student(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cohort = models.ForeignKey(Cohort, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

class Module(models.Model): #CA4094
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=150)
    cohort = models.ManyToManyField(Cohort)
    has_teacher = models.BooleanField(default=False)

    def __str__(self):
        return self.code

class Teacher(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    module = models.ManyToManyField(Module, blank=True)

    def __str__(self):
        return self.user.username

class Room(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.CharField(max_length=150)
    number_of_seats = models.IntegerField()

    def __str__(self):
        return self.number

class Booking(models.Model):
    id = models.AutoField(primary_key=True)
    DAY_CHOICES = [("Monday","Monday"), ("Tuesday","Tuesday"), ("Wednesday","Wednesday"), ("Thursday","Thursday"), ("Friday","Friday")]
    day = models.CharField(choices=DAY_CHOICES, max_length=150)
    HOUR_CHOICES = [(datetime.time(hour=x), '{:02d}:00'.format(x)) for x in range(7, 19)]
    start_time = models.TimeField(choices=HOUR_CHOICES)
    duration = models.IntegerField(default=1)
    CLASS_TYPES=[('Lecture', 'Lecture'),('Lab', 'Lab'),('Tutorial', 'Tutorial'),('Seminar', 'Seminar')]
    type = models.CharField(choices=CLASS_TYPES, max_length=150)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'Booking {self.id}'

class RoomBooking(models.Model):
    id = models.AutoField(primary_key=True)
    DAY_CHOICES = [("Monday","Monday"), ("Tuesday","Tuesday"), ("Wednesday","Wednesday"), ("Thursday","Thursday"), ("Friday","Friday")]
    day = models.CharField(choices=DAY_CHOICES, max_length=150)
    HOUR_CHOICES = [(datetime.time(hour=x), '{:02d}:00'.format(x)) for x in range(7, 19)]
    start_time = models.TimeField(choices=HOUR_CHOICES)
    duration = models.IntegerField(default=1)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'Booking {self.id}'